# GIT SUMMARY
## What is GIT?
- Version controlled software for easy collaboration between the team members.
- Track record of every improvement by every team member during software development is maintained. 
## Common Terms
1. **Repository:**
- A repository (Repo) is the collection of files and folders (code files) that you’re using git to track. 
- Any team member can contribute in the repo.
2. **GitLab:**
- Remote storage solution for repos.
3. **Commit:**
- Similar to saving your work.
- Unless pushed in remote repository, the work is saved only on local machine.
4. **Push:**
-  Syncing your commits to GitLab.
5. **Branch:**
- These are separate instances of the code that is different from the main codebase.
- Consider git repo as a tree. The trunk of the tree, the main software, is called the Master Branch. 
6. **Merge:**
- Integrating two branches together to become part of the primary codebase when a branch is error free. 
7. **Clone:**
- Making an exact copy of an online repo on your local machine.
8. **Fork:**
- Create an entirely new repo of that code under your own name.
## Git Internals
### States of Files
1. **Modified** - Editted a file but not committed it.
2. **Staged** - Marked a modified file in its current version to go into your next picture/snapshot.
3. **Committed** - Data is safely stored in your local repo in form of pictures/snapshots.
### Trees and commands to move files within these trees
![image](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/extras/Git.png)
### Git Work Flow
1. Clone
2. New Branch
3. Modify files in your working tree.
4. Staging
5. Commit
6. Push

# GIT LAB SUMMARY
## Merge Request Workflow
1. Create a **Branch**.
2. Work on code for a particular **issue**.
3. Code/work should satisfy all contents in the **checklist**.
4. Submit the **merge request**.
5. After the review make the suggested changes.

