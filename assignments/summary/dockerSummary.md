# DOCKER SUMMARY
## Formatting styles

| Format | Style |
| ------------ | ------------- |
| Bold | ** ** |
| Italics | _ _ |
| Bold,Italics | _** **_ |
| # | Heading |
| Link | [name] (url) |
| Image | ![image_name] (url) |
| Blockquotes | > |
| List | * |
